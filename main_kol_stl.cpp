#include <iostream>
#include "inc/stos_tab_ink.hh"
#include <queue>

using namespace std;

void Usun_all(queue <int>* kolejka)
{
    while( kolejka->empty() == false )
        {
            kolejka->pop();
        }
}

void Wyswietl (queue <int> kolejka)
{
    queue <int> kolejka1 = kolejka;
    int i = kolejka1.size();
    if (i==0)
        cout << "kolejka jest pusta" << endl;
    else
        for (i;i>0;i--)
        {
             cout << kolejka1.front() << endl;
            kolejka1.pop();
        }
}

int main()
{
    queue <int> kolejka;
    int opcja=5;
    int element=0;
    do
    {
        cout << "0 - wyjscie z programu" << endl;
        cout << "1 - dodaj element do kolejki" << endl;
        cout << "2 - usun element z kolejki" << endl;
        cout << "3 - wyswietl kolejki" << endl;
        cout << "4 - usun caly kolejki" << endl;
        cin >> opcja;
        cout << endl;

        switch (opcja)
        {
        case 0: break;
        case 1: {
            cout << "podaj liczbe calkowita, ktora zostanie dolozona do stosu"<<endl;
            cin >> element;
            cout << endl;
            kolejka.push(element);
            break;
        }
        case 2: kolejka.pop(); break;
        case 3: Wyswietl(kolejka); break;
        case 4: Usun_all(&kolejka); break;
        }
    } while (opcja>0);
        return 0;

}


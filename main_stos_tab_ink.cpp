#include <iostream>
#include "inc/stos_tab_ink.hh"

using namespace std;


int main()
{
        Stos stos;
        int opcja=5;
        int element=0;
        do
        {
            cout << "0 - wyjscie z menu listy" << endl;
            cout << "1 - dodaj element do listy" << endl;
            cout << "2 - usun element z listy" << endl;
            cout << "3 - wyswietl liste" << endl;
            cout << "4 - usun cala liste" << endl;
            cin >> opcja;
            cout << endl;

            switch (opcja)
            {
            case 0: break;
            case 1: {
                cout << "podaj liczbe calkowita, ktora zostanie dolozona do stosu"<<endl;
                cin >> element;
                cout << endl;
                stos.Dodaj(element);
                break;
            }
            case 2: stos.Usun_1(); break;
            case 3: stos.Wyswietl(); break;
            case 4: stos.Usun_all(); break;
            }
        } while (opcja>0);

}


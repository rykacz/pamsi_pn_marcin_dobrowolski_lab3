#ifndef KOL_TAB_INK_H
#define KOL_TAB_INK_H
#include <iostream>

using namespace std;

class Kolejka
{
private:
    int pojemnosc;
    int* k; //tablica kolejki
    int r; //koniec kolejki (miejsce nastepnego elementu
    int f; // poczatek kolejki
public:
    Kolejka(); //konstruktor
    ~Kolejka(); //destruktor
    int size() const;
    bool pusta() const; //funkcja do sprawdzenia czy jest element w liscie
    void Dodaj(const int& elem); // dodawanie na poczatek listy
    void Usun_1(); //usuwanie z poczatku listy
    void Wyswietl() const; //wyswietlanie listy
    void Usun_all(); //usuwanie listy

};

#endif // KOL_TAB_INK_H

#ifndef STOS_TAB_INK_HH
#define STOS_TAB_INK_HH
#include <iostream>

using namespace std;
class Stos
{
public:
    Stos(); //konstruktor
    ~Stos(); //destruktor
    bool pusty() const; //funkcja do sprawdzenia czy jest element w stosie
    void Dodaj(const int& elem); // dodawanie na poczatek stosu
    void Usun_1(); //usuwanie z poczatku stosu
    void Wyswietl() const; //wyswietlanie stosu
    void Usun_all(); //usuwanie wszsystkich elementow stosu

private:
    int pojemnosc; //pojemnosc stosu
    int* s; //tablica stosu
    int top; //gora stosu
};

#endif //

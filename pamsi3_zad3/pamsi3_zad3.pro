TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    stosy/main_stos_stl.cpp \
    stosy/main_stos_tab_ink.cpp \
    stosy/src/stos_tab_ink.cpp \
    kolejki/src/kol_tab_ink.cpp \
    kolejki/main_kol_stl.cpp \
    kolejki/main_kol_tab_ink.cpp \
    stosy/main_stos_tab_pod.cpp \
    stosy/main_stos_lista.cpp \
    stosy/src/stos_tab_pod.cpp \
    stosy/src/lista.cpp \
    kolejki/src/kol_tab_pod.cpp \
    kolejki/main_kol_tab_pod.cpp \
    kolejki/src/kolejka.cpp \
    kolejki/main_kol_lista.cpp

HEADERS += \
    stosy/inc/stos_tab_ink.hh \
    kolejki/inc/kol_tab_ink.hh \
    stosy/inc/stos_tab_pod.hh \
    stosy/inc/lista.hh \
    kolejki/inc/kolejka.hh


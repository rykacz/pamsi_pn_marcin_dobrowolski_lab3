
#include <iostream>
#include "../inc/kolejka.hh"

using namespace std;

Kolejka::Kolejka() {head= NULL; end=NULL;} //konstuktor - head to null

Kolejka::~Kolejka() //destruktor
{
    delete head;
}

void Kolejka::Dodaj(const int& e)//dodawanie do listy na koniec
{
    Wezel_k* dodawany = new Wezel_k; // tworzy nowy wezel
    dodawany->element = e; //zapis danych do wezla dodawanego
    dodawany->next=NULL; // -||-

    if (head==NULL) // jezeli kolejka pusta to tylko dodaje
    {
        head=dodawany;
        end=dodawany;
    }
    else
    {
        end->next=dodawany;
        end=dodawany;
    }
}

bool Kolejka::pusta() const
{
    if (head==NULL)
        return true;
    else
        return false;
}

void Kolejka::Usun_1()
{
    if (pusta() == true)
    {
        cout << "ERROR - PUSTA Kolejka!!!" << endl;
        return;
    }
    Wezel_k* old = head; //zapis biezacego head;
    head=old->next; //ominiecie starego head;
    delete old; // usun old
}

const int& Kolejka::pierwszy() const
{
    Wezel_k* pierwszy = head;
    return pierwszy->element;
    delete pierwszy;
}

void Kolejka::Wyswietl() const
{
    int flaga=0; // flaga do znakowania konca petli
    Wezel_k* tmp1=new Wezel_k; //wezel tmp1
    tmp1=head;
    if (pusta()==true)
    {
        cout << "Kolejka jest pusta" << endl;
    }
    else
    {
    do
    {
        if (tmp1->next == NULL) //sprawdz czy to ostatni wezel
        {
            if(flaga<1)
                cout << tmp1->element << endl; //wyswietl ostatni element
            flaga=1; //podnies flage jak tak
        }
        if (flaga==0)
        {
            cout << tmp1->element << endl; //wyswietl element wezla
            tmp1 = tmp1->next; //tmp1 to tmp2 czyli nastepny wezel do wyswietlenia
        }

    }
    while (flaga==0);
    }
}

void Kolejka::Usun_all()
{
    int  flaga = 0;
    do
    {
        if (head == NULL) //sprawdz czy to ostatni wezel
            flaga=1; //podnies flage jak tak
        else
            Usun_1();
    }
    while (flaga==0);
}

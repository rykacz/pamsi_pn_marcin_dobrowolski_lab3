
#include <iostream>
#include "../inc/kol_tab_ink.hh"

using namespace std;

Kolejka::Kolejka() //konstuktor - head to null
{
    pojemnosc=5; //pojemnosc startowa i o tyle samo powiekszan
    k = new int [pojemnosc];
    r=0; //koniec kolejki
    f=0; //poczatek kolejki
}

Kolejka::~Kolejka() //destruktor
{
    delete [] k;
}

int Kolejka::size() const
{
    return (pojemnosc-f+r)%pojemnosc;
}

void Kolejka::Dodaj(const int& e)
{
    if (size()==pojemnosc-1) //sprawdzam czy jest miejsce
    {
        int *tmp = new int [pojemnosc]; //tab tmp
        for (int i=0;i<pojemnosc;i++)
            tmp[i]=k[i];//przekopiowiuje do tablicy tmp
        delete [] k;
        k = new int [pojemnosc+5];//nowa tablica Kolejkau
        for (int i=0;i<pojemnosc;i++)
            k[i]=tmp[i];//przekopiowiuje do tablicy Kolejkau
        delete [] tmp;
        pojemnosc=pojemnosc+5;
    }
    k[r]=e;
    r=(r+1)%pojemnosc;
}

void Kolejka::Usun_1()
{
    if(f==r)
    {
        cout << "ERROR - PUSTY Kolejka!!!" << endl;
        return;
    }
    else
        f=(f+1)%pojemnosc;
}

void Kolejka::Wyswietl() const
{
    int i=f;
    if(i==r)
    {
        cout << "ERROR - PUSTA Kolejka!!!" << endl;
        return;
    }
    else
    {
        int i=f;
        while (i!=r)
        {
            cout << k[i] << endl;
            i = (i+1) % pojemnosc;
        }
    }
}

void Kolejka::Usun_all()
{
    r=0;
    f=0;
    delete [] k;
}



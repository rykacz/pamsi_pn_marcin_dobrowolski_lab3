#include <iostream>
#include "inc/kol_tab_pod.hh"
#include <sys/time.h>
#include <cstdlib>

using namespace std;


int main()
{
        Kolejka Kolejka;
        int ilosc;
        int element=0;
        timeval t1, t2;
        double elapsedTime;
        srand( time( NULL ) );


        cout << "Podaj ilosc elementow do losowania do Kolejki" << endl;
        cin >> ilosc;

        gettimeofday(&t1, NULL); //start stoper
        for (int i = 0; i<ilosc; i++)
        {
            element= rand() % 10; // losujemy cyfre
            Kolejka.Dodaj(element); //dodajemy do Kolejkau
        }
        gettimeofday(&t2, NULL); //stop stoper
        cout << endl;
        elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
        elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
        cout << "czas operacji to " << elapsedTime << " ms"<<endl;

}



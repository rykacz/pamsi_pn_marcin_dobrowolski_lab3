#ifndef KOLEJKA_HH
#define KOLEJKA_HH

#include <iostream>
using namespace std;

class Wezel_k
{
private:
    int element; //wartosc elementu
    Wezel_k* next; //wskaznik na nastepny element;
    friend class Kolejka; //dostep dla listy
};


class Kolejka
{
private:
    Wezel_k* head; //wskaznik na poczatek listy
    Wezel_k* end; //wskaznik na koniec kolejki
public:
    Kolejka(); //konstruktor
    ~Kolejka(); //destruktor
    bool pusta() const; //funkcja do sprawdzenia czy jest element w liscie
    const int& pierwszy() const; //zwraca pierwszy element
    void Dodaj(const int& elem); // dodawanie na poczatek listy
    void Usun_1(); //usuwanie z poczatku listy
    void Wyswietl() const; //wyswietlanie listy
    void Usun_all(); //usuwanie listy

};

#endif

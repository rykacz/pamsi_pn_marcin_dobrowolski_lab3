
#include <iostream>
#include "../inc/stos_tab_pod.hh"

using namespace std;

Stos::Stos() //konstuktor - head to null
{
    pojemnosc=5; //pojemnosc startowa i o tyle samo powiekszan
    s = new int [pojemnosc];
    top=-1;
}

Stos::~Stos() //destruktor
{
    delete [] s;
}

void Stos::Dodaj(const int& e)
{
    if (top==pojemnosc-1) //sprawdzam czy jest miejsce
    {
        int *tmp = new int [pojemnosc]; //tab tmp
        for (int i=0;i<pojemnosc;i++)
            tmp[i]=s[i];//przekopiowiuje do tablicy tmp
        delete [] s;
        s = new int [pojemnosc+pojemnosc];//nowa tablica stosu
        for (int i=0;i<pojemnosc;i++)
            s[i]=tmp[i];//przekopiowiuje do tablicy stosu
        pojemnosc= pojemnosc + pojemnosc;
        delete [] tmp;
    }
    top = top+1;
    s[top] = e;
}

void Stos::Usun_1()
{
    if(top<0)
    {
        cout << "ERROR - PUSTY STOS!!!" << endl;
        return;
    }
    else
        top--;
}

void Stos::Wyswietl() const
{
    if(top<0)
    {
        cout << "ERROR - PUSTY STOS!!!" << endl;
        return;
    }
    else
        for (int i=top; i>=0; i--)
            cout << s[i] <<endl;
}

void Stos::Usun_all()
{
    top=-1;
    delete [] s;
}


#ifndef LISTA_HH
#define LISTA_HH

#include <iostream>
using namespace std;

class Wezel
{
private:
    int element; //wartosc elementu
    Wezel* next; //wskaznik na nastepny element;
    friend class Lista; //dostep dla listy
};


class Lista
{
private:
    Wezel* head; //wskaznik na poczatek listy
public:
    Lista(); //konstruktor
    ~Lista(); //destruktor
    bool pusta() const; //funkcja do sprawdzenia czy jest element w liscie
    const int& pierwszy() const; //zwraca pierwszy element
    void Dodaj(const int& elem); // dodawanie na poczatek listy
    void Usun_1(); //usuwanie z poczatku listy
    void Wyswietl() const; //wyswietlanie listy
    void Usun_all(); //usuwanie listy

};

#endif

#include <iostream>
#include "inc/stos_tab_ink.hh"
#include <stack>
#include <sys/time.h>
#include <cstdlib>


using namespace std;

void Usun_all(stack <int>* stos)
{
    while( stos->empty() == false )
        {
            stos->pop();
        }
}

void Wyswietl (stack <int> stos)
{
    stack <int> stos1 = stos;
    int i = stos1.size();
    if (i==0)
        cout << "Stos jest pusty" << endl;
    else
        for (i;i>0;i--)
        {
             cout << stos1.top() << endl;
            stos1.pop();
        }
}

int main()
{
    stack <int> stos;
    int ilosc;
    int element=0;
    timeval t1, t2;
    double elapsedTime;
    srand( time( NULL ) );


    cout << "Podaj ilosc elementow do losowania do stosu" << endl;
    cin >> ilosc;

    gettimeofday(&t1, NULL); //start stoper
    for (int i = 0; i<ilosc; i++)
    {
        element= rand() % 10; // losujemy cyfre
        stos.push(element); //dodajemy do stosu
    }
    gettimeofday(&t2, NULL); //stop stoper
    cout << endl;
    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
    cout << "czas operacji to " << elapsedTime << " ms"<<endl;
    return 0;
}

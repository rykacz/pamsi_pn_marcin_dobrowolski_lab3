#include <iostream>
#include "inc/stos_tab_ink.hh"
#include <stack>

using namespace std;

void Usun_all(stack <int>* stos)
{
    while( stos->empty() == false )
        {
            stos->pop();
        }
}

void Wyswietl (stack <int> stos)
{
    stack <int> stos1 = stos;
    int i = stos1.size();
    if (i==0)
        cout << "Stos jest pusty" << endl;
    else
        for (i;i>0;i--)
        {
             cout << stos1.top() << endl;
            stos1.pop();
        }
}

int main()
{
    stack <int> stos;
    int opcja=5;
    int element=0;
    do
    {
        cout << "0 - wyjscie z programu" << endl;
        cout << "1 - dodaj element do stosu" << endl;
        cout << "2 - usun element z stosu" << endl;
        cout << "3 - wyswietl stos" << endl;
        cout << "4 - usun caly stos" << endl;
        cin >> opcja;
        cout << endl;

        switch (opcja)
        {
        case 0: break;
        case 1: {
            cout << "podaj liczbe calkowita, ktora zostanie dolozona do stosu"<<endl;
            cin >> element;
            cout << endl;
            stos.push(element);
            break;
        }
        case 2: stos.pop(); break;
        case 3: Wyswietl(stos); break;
        case 4: Usun_all(&stos); break;
        }
    } while (opcja>0);
        return 0;

}
